﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AcerCMS.Models
{
    [MetadataType(typeof(CommentMetadata))]
    public partial class Comment
    {
    }

    class CommentMetadata
    {
        [Display(Name="Yazar")]
        [StringLength(50, ErrorMessage = "{0} en fazla {1} karakter olabilir!")]
        [Required(ErrorMessage = "{0} Gerekli!")]
        public string AuthorName { get; set; }

        [Display(Name = "Yorum")]
        [StringLength(400, ErrorMessage = "{0} en fazla {1} karakter olabilir!")]
        [Required(ErrorMessage = "{0} Gerekli!")]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        [DataType(DataType.DateTime)]
        public System.DateTime UpdateDate { get; set; }

        public bool IsSpam { get; set; }

        [StringLength(50)]
        [Required]
        public string Entity { get; set; }
        
        [Required]
        public int EntityId { get; set; }
    }    
}